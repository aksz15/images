// 00 Alles löschen
CALL apoc.periodic.iterate('MATCH (n) RETURN n', 'DETACH DELETE n', {batchSize:1000});
CALL apoc.schema.assert({},{},true) YIELD label, key RETURN *;

// 01 Ethnische Gruppen
// Prepare a SPARQL query 
WITH 'SELECT ?item ?itemLabel 
      WHERE{ ?item wdt:P31 wd:Q989255 . 
      SERVICE wikibase:label { bd:serviceParam wikibase:language 
      "[AUTO_LANGUAGE],en" } }' AS sparql 
// make a request to Wikidata
CALL apoc.load.jsonParams('https://query.wikidata.org/sparql?query=' + 
                           apoc.text.urlencode(sparql), 
                         { Accept: "application/sparql-results+json"}, null) 
YIELD value 
// Unwind results to row 
UNWIND value['results']['bindings'] as row 
// Prepare data 
WITH row['itemLabel']['value'] as race, 
     row['item']['value'] as url, 
     split(row['item']['value'],'/')[-1] as id 
// Store to Neo4j 
CREATE (r:Race) SET r.race = race, 
                    r.url = url, 
                    r.id = id;

// 02 Personen importieren
// Iterate over each race in graph
MATCH (r:Race)
// Prepare a SparQL query
WITH 'SELECT ?item ?itemLabel 
      WHERE { ?item wdt:P31 wd:' + r.id + ' . 
      SERVICE wikibase:label { bd:serviceParam wikibase:language 
      "[AUTO_LANGUAGE],en" } }' AS sparql, r 
// make a request to Wikidata 
CALL apoc.load.jsonParams( "https://query.wikidata.org/sparql?query=" + 
                            apoc.text.urlencode(sparql), 
                            { Accept: "application/sparql-results+json"}, null)
YIELD value 
UNWIND value['results']['bindings'] as row 
WITH row['itemLabel']['value'] as name, 
     row['item']['value'] as url, 
     split(row['item']['value'],'/')[-1] as id, r 
// Store to Neo4j 
MERGE (c:Character {id:id}) 
SET c.label = name, 
    c.url = url
CREATE (c)-[:BELONG_TO]->(r);

// Ethnische Gruppenmitglieder
MATCH (r:Race) 
RETURN r.race as race, 
       size((r)<-[:BELONG_TO]-()) as members 
ORDER BY members DESC 
LIMIT 10;

// 03 Gender, Land und Tod
// Iterate over characters 
MATCH (r:Character) 
// Prepare a SparQL query 
WITH 'SELECT * 
      WHERE{ ?item rdfs:label ?name . 
              filter (?item = wd:' + r.id + ') 
              filter (lang(?name) = "en" ) . 
      OPTIONAL{ ?item wdt:P21 [rdfs:label ?gender] . 
                filter (lang(?gender)="en") } 
      OPTIONAL{ ?item wdt:P27 [rdfs:label ?country] . 
              filter (lang(?country)="en") } 
      OPTIONAL{ ?item wdt:P1196 [rdfs:label ?death] . 
              filter (lang(?death)="en") }}' AS sparql, r 
// make a request to Wikidata 
CALL apoc.load.jsonParams( "https://query.wikidata.org/sparql?query=" 
    + apoc.text.urlencode(sparql), 
    { Accept: "application/sparql-results+json"}, null)
YIELD value 
UNWIND value['results']['bindings'] as row 
SET r.gender = row['gender']['value'], 
    r.manner_of_death = row['death']['value'] 
// Execute FOREACH statement 
FOREACH(ignoreme in case when row['country'] is not null then [1] else [] end | 
    MERGE (c:Country{label:row['country']['value']}) 
    MERGE (r)-[:IN_COUNTRY]->(c));
    
// manner_of_death_query
MATCH (n:Character) 
WHERE exists (n.manner_of_death) 
RETURN n.manner_of_death as manner_of_death, 
       count(*) as count;

// country_info_query
MATCH (c:Country)
RETURN c.label as country, 
       size((c)<-[:IN_COUNTRY]-()) as members
ORDER BY members DESC 
LIMIT 10;

// 04 Verwandtschaft
// Iterate over characters 
MATCH (r:Character) 
WITH 'SELECT * 
      WHERE{ ?item rdfs:label ?name . 
             filter (?item = wd:' + r.id + ') 
             filter (lang(?name) = "en" ) . 
      OPTIONAL{ ?item wdt:P22 ?father } 
      OPTIONAL{ ?item wdt:P25 ?mother } 
      OPTIONAL{ ?item wdt:P1038 ?relative } 
      OPTIONAL{ ?item wdt:P3373 ?sibling } 
      OPTIONAL{ ?item wdt:P26 ?spouse }}' AS sparql, r 
// make a request to wikidata 
CALL apoc.load.jsonParams( "https://query.wikidata.org/sparql?query=" + 
    apoc.text.urlencode(sparql), 
    { Accept: "application/sparql-results+json"}, null) YIELD value 
UNWIND value['results']['bindings'] as row 
FOREACH(ignoreme in case when row['mother'] is not null then [1] else [] end | 
    MERGE (c:Character{url:row['mother']['value']}) 
    MERGE (r)-[:HAS_MOTHER]->(c)) 
FOREACH(ignoreme in case when row['father'] is not null then [1] else [] end | 
    MERGE (c:Character{url:row['father']['value']}) 
    MERGE (r)-[:HAS_FATHER]->(c)) 
FOREACH(ignoreme in case when row['relative'] is not null then [1] else [] end | 
    MERGE (c:Character{url:row['relative']['value']}) 
    MERGE (r)-[:HAS_RELATIVE]-(c)) 
FOREACH(ignoreme in case when row['sibling'] is not null then [1] else [] end | 
    MERGE (c:Character{url:row['sibling']['value']}) 
    MERGE (r)-[:SIBLING]-(c))
FOREACH(ignoreme in case when row['spouse'] is not null then [1] else [] end | 
    MERGE (c:Character{url:row['spouse']['value']}) 
    MERGE (r)-[:SPOUSE]-(c));
    
// multiple_spouses_query
MATCH p=(a)-[:SPOUSE]-(b)-[:SPOUSE]-(c) 
RETURN p LIMIT 10;

// multiple_kids_query
MATCH (c:Character)<-[:HAS_FATHER|HAS_MOTHER]-()-[:HAS_FATHER|HAS_MOTHER]->(other) 
WITH c, collect(distinct other) as others 
WHERE size(others) > 1 
MATCH p=(c)<-[:HAS_FATHER|HAS_MOTHER]-()-[:HAS_FATHER|HAS_MOTHER]->() 
RETURN p;

// sibling_candidate_query
MATCH p=(a:Character)-[:HAS_FATHER|:HAS_MOTHER]->()<-[:HAS_FATHER|:HAS_MOTHER]-(b:Character) 
WHERE NOT (a)-[:SIBLING]-(b) 
RETURN p LIMIT 5;

// sibling_populate_query
MATCH p=(a:Character)-[:HAS_FATHER|:HAS_MOTHER]->()<-[:HAS_FATHER|:HAS_MOTHER]-(b:Character) 
WHERE NOT (a)-[:SIBLING]-(b) 
MERGE (a)-[:SIBLING]-(b);

// country_populate_query
MATCH (country)<-[:IN_COUNTRY]-(s:Character)-[:SIBLING]-(t:Character) 
WHERE NOT (t)-[:IN_COUNTRY]->() 
MERGE (t)-[:IN_COUNTRY]->(country);

// 05 Gruppen und Rollen
MATCH (r:Character) 
WHERE exists (r.id) 
WITH 'SELECT * 
      WHERE{ ?item rdfs:label ?name . 
             filter (?item = wd:' + r.id + ') 
             filter (lang(?name) = "en" ) . 
      OPTIONAL { ?item wdt:P106 [rdfs:label ?occupation ] . 
           filter (lang(?occupation) = "en" ). } 
      OPTIONAL { ?item wdt:P103 [rdfs:label ?language ] . 
           filter (lang(?language) = "en" ) . } 
      OPTIONAL { ?item wdt:P463 [rdfs:label ?member_of ] . 
           filter (lang(?member_of) = "en" ). } 
      OPTIONAL { ?item wdt:P1344[rdfs:label ?participant ] . 
           filter (lang(?participant) = "en") . } 
      OPTIONAL { ?item wdt:P39[rdfs:label ?position ] . 
           filter (lang(?position) = "en") . }}' AS sparql, r 
CALL apoc.load.jsonParams( "https://query.wikidata.org/sparql?query=" + 
                             apoc.text.urlencode(sparql), 
                             { Accept: "application/sparql-results+json"}, null) 
YIELD value 
UNWIND value['results']['bindings'] as row 
FOREACH(ignoreme in case when row['language'] is not null then [1] else [] end | 
        MERGE (c:Language{label:row['language']['value']}) 
        MERGE (r)-[:HAS_LANGUAGE]->(c)) 
FOREACH(ignoreme in case when row['occupation'] is not null then [1] else [] end | 
        MERGE (c:Occupation{label:row['occupation']['value']}) 
        MERGE (r)-[:HAS_OCCUPATION]->(c)) 
FOREACH(ignoreme in case when row['member_of'] is not null then [1] else [] end | 
        MERGE (c:Group{label:row['member_of']['value']}) 
        MERGE (r)-[:MEMBER_OF]->(c)) 
FOREACH(ignoreme in case when row['participant'] is not null then [1] else [] end | 
        MERGE (c:Event{label:row['participant']['value']}) 
        MERGE (r)-[:PARTICIPATED]->(c)) 
SET r.position = row['position']['value'];

// investigate_groups_query
MATCH (n:Group)<-[:MEMBER_OF]-(c)
OPTIONAL MATCH (c)-[:HAS_OCCUPATION]->(o) 
RETURN n.label as group, 
       count(*) as size, 
       collect(c.label)[..3] as members, 
       collect(distinct o.label)[..3] as occupations 
ORDER BY size DESC;

// 06 Feinde
MATCH (r:Character) 
WHERE exists (r.id) 
WITH 'SELECT * 
      WHERE { ?item rdfs:label ?name . 
              filter (?item = wd:' + r.id + ') 
              filter (lang(?name) = "en" ) . 
      OPTIONAL{ ?item wdt:P1830 [rdfs:label ?owner ] . 
            filter (lang(?owner) = "en" ). } 
      OPTIONAL{ ?item wdt:P7047 ?enemy }}' AS sparql, r 
CALL apoc.load.jsonParams( "https://query.wikidata.org/sparql?query=" + 
                            apoc.text.urlencode(sparql), 
                            { Accept: "application/sparql-results+json"}, null) 
YIELD value 
WITH value,r 
WHERE value['results']['bindings'] <> [] 
UNWIND value['results']['bindings'] as row 
FOREACH(ignoreme in case when row['owner'] is not null then [1] else [] end |
    MERGE (c:Item{label:row['owner']['value']}) 
    MERGE (r)-[:OWNS_ITEM]->(c)) 
FOREACH(ignoreme in case when row['enemy'] is not null then [1] else [] end | 
    MERGE (c:Character{url:row['enemy']['value']}) 
    MERGE (r)-[:ENEMY]->(c));

// family_enemy_query
MATCH p=(a)-[:SPOUSE|SIBLING|HAS_FATHER|HAS_MOTHER]-(b) 
WHERE (a)-[:ENEMY]-(b) 
RETURN p;

// family_enemy_2hops_query
MATCH p=(a)-[:SPOUSE|SIBLING|HAS_FATHER|HAS_MOTHER*..2]-(b) 
WHERE (a)-[:ENEMY]-(b) 
RETURN p;

// ########################################################################################################
// project_graph
CALL gds.graph.create('family','Character', 
    ['SPOUSE','SIBLING','HAS_FATHER','HAS_MOTHER']);

// wcc_stats_query  
CALL gds.wcc.stats('family') 
YIELD componentCount, 
      componentDistribution 
RETURN componentCount as components, 
       componentDistribution.p75 as p75, 
       componentDistribution.p90 as p90, 
       apoc.math.round(componentDistribution.mean,2) as mean, 
       componentDistribution.max as max;
       
// wcc_write_query
CALL gds.wcc.write('family', {writeProperty:'familyComponent'});

// top5_families_query
MATCH (c:Character) 
OPTIONAL MATCH (c)-[:BELONG_TO]->(race) 
WITH c.familyComponent as familyComponent, 
     count(*) as size, 
     collect(c.label) as members, 
     collect(distinct race.race) as family_race 
ORDER BY size DESC LIMIT 5 
RETURN familyComponent, 
       size, 
       members[..3] as random_members, 
       family_race;
       
// ir_query
MATCH (c:Character) 
WHERE c.familyComponent = 166 // fix the family component 
MATCH p=(race)<-[:BELONG_TO]-(c)-[:SPOUSE]-(other)-[:BELONG_TO]->(other_race) 
WHERE race <> other_race AND id(c) > id(other) 
RETURN c.label as spouse_1, 
       race.race as race_1, 
       other.label as spouse_2, 
       other_race.race as race_2;
       
// oldest_halfelf_query
MATCH (c:Character)
WHERE (c)-[:BELONG_TO]->(:Race{race:'half-elven'})
MATCH p=(c)<-[:HAS_FATHER|HAS_MOTHER*..20]-(end)
WHERE NOT (end)<-[:HAS_FATHER|:HAS_MOTHER]-()
WITH c, max(length(p)) as descendants
ORDER BY descendants DESC
LIMIT 5
RETURN c.label as character,
       descendants;

// betwenness_centrality_query
CALL gds.alpha.betweenness.stream({ 
    nodeQuery:"MATCH (n:Character) WHERE n.familyComponent = 166 
               RETURN id(n) as id", 
    relationshipQuery:"MATCH (s:Character)-[:HAS_FATHER|HAS_MOTHER|SPOUSE|SIBLING]-(t:Character) 
                       RETURN id(s) as source, id(t) as target", 
    validateRelationships:false})
YIELD nodeId, centrality
RETURN gds.util.asNode(nodeId).label as character,
        centrality
ORDER BY centrality DESC 
LIMIT 10;



